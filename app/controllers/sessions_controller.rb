class SessionsController < ApplicationController
  def new
    render :new
  end
  
  def create
    @user = User.find_by_credentials(
      params[:user][:username],
      params[:user][:password]
    )
    
    if @user
      login!(@user)
      redirect_to goals_url
    else
      flash.now[:errors] = ["Invalid Username/Password combination"]
      render :new
    end
  end
  
  def destroy
    log_out!
    redirect_to goals_url
  end
end

