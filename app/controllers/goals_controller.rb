class GoalsController < ApplicationController
  
  before_filter :require_login
  
  def new
    @goal = Goal.new
    render :new    
  end

  def create
    @goal = current_user.goals.new(goal_params)
    
    if @goal.save
      redirect_to goal_url(@goal)
    else
      flash.now[:errors] = @goal.errors.full_messages
      render :new
    end
  end
  
  
  def index
    @goals = Goal.all
    render :index
  end

  def edit
    @goal = Goal.find(params[:id])
    if @goal
      render :edit
    else
      flash.now[:errors] = ["Invalid goal"]
      redirect_to goals_url
    end
  end
  
  def update
    @goal = Goal.find(params[:id])
    
    if @goal.update(goal_params)
      redirect_to goal_url(@goal)
    else
      flash.now[:errors] = @goal.errors.full_messages
      render :edit
    end
  end

  def show
    @goal = Goal.find_by_id(params[:id])
    render :show
  end
  
  def destroy
    goal = Goal.find(params[:id])
    if current_user.id == goal.user_id
      goal.destroy
    end
    redirect_to goals_url
  end
  
  private
  
  def goal_params
    params.require(:goal).permit(:name, :privacy, :completion)
  end
end
