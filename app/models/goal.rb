class Goal < ActiveRecord::Base
  validates :name, :user, presence: true
  validates :privacy, :completion, inclusion: { in: [true, false] }
  
  belongs_to :user
end
