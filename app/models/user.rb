class User < ActiveRecord::Base
  attr_reader :password
  validates :username, :password_digest, :session_token, presence: true
  validates :password, length: { minimum: 6, allow_nil: true}
  
  has_many :goals
  
  before_validation do
    ensure_session_token
  end
  
  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end
  
  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end
  
  def self.generate_session_token
    SecureRandom.urlsafe_base64(16)
  end
  
  def self.find_by_credentials(username, password)

    found_user = User.find_by_username(username)
    if found_user && found_user.is_password?(password)
       found_user
    else
       nil
    end
  end
  
  def reset_session_token!
    self.session_token = self.class.generate_session_token
    self.save!
    self.session_token
  end
  
  
  private
  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end
end
