def sign_in(username, password)
  fill_in "Username", with: username
  fill_in "Password", with: password
  click_button 'Sign In'
  expect(page).to have_content(username)
end

def sign_up(username, password)
  fill_in "Username", with: username
  fill_in "Password", with: password
  click_button 'Sign Up'
  expect(page).to have_content(username)
end