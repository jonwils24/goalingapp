def make_two
    visit '/users/new'
    sign_up("helloworld", "password")
    click_link 'New Goal'
    fill_in 'Name', with: 'Finish goals app'
    choose('true')#goal is set to private
    click_button 'Create Goal'
    click_link 'Goal Index'
    click_link 'New Goal'
    fill_in 'Name', with: 'Do tonights readings'
    choose('false')#goal is set to private
    click_button 'Create Goal'
end

def batman_signup
  #save_and_open_page
  click_button 'Sign Out'
  visit '/users/new'
  sign_up("batman", "password")
end