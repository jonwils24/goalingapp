require "spec_helper"


feature "Create Goals" do 
  before :each do
    visit 'users/new'
    sign_up("helloworld", "password")
    click_link 'New Goal'
  end
  
  it "has a new goal page" do
    expect(page).to have_content 'New Goal'
    expect(page).to have_button 'Create Goal'
  end
  
  it "takes a name and privacy" do 
    expect(page).to have_field 'Name'
    expect(page).to have_content 'Private' #radio link
  end
  
  
  it "can create a goal" do 
    fill_in 'Name', with: 'Finish goals app'
    choose('true')
    click_button 'Create Goal'
    expect(page).to have_content 'Finish goals app'
  end
  
  it "validates name" do
    choose('true')
    click_button 'Create Goal'
    expect(page).to have_content "Name can't be blank"
  end
  
  it "redirects to goal show page" do
    fill_in 'Name', with: 'Finish goals app'
    choose('true')
    click_button 'Create Goal'
    expect(page).to have_content 'Finish goals app'
  end
  
  it "can't create goal unless logged in" do
    click_button 'Sign Out'
    visit '/goals/new'
    expect(page).to have_content 'Username'
    expect(page).to have_content 'Password'
  end
end

feature "Read Goals" do 
  before :each do
    visit 'users/new'
    sign_up("helloworld", "password")
    click_link 'New Goal'
    fill_in 'Name', with: 'Finish goals app'
    choose('true')#goal is set to private
    click_button 'Create Goal'
    click_link 'Goal Index'
    click_link 'New Goal'
    fill_in 'Name', with: 'Do tonights readings'
    choose('false')#goal is set to private
    click_button 'Create Goal'
  end
  
  it "has a name" do 
    expect(page).to have_content 'Do tonights readings'
  end
  
  it "has a completion status" do 
    expect(page).to have_content 'false'
  end
  
  it "can update completion status" do 
    expect(page).to have_button 'Goal Complete'
    click_button 'Goal Complete'
    expect(page).to have_content 'true'
  end
  
  it "another user can't see other user's private goals" do 
    batman_signup
    expect(page).not_to have_content 'Finish goals app'
  end
  
  it "can see public goals" do 
    batman_signup
    expect(page).to have_content 'Do tonights readings'
  end
  
end

feature "Edit Goals" do 
  before :each do
    make_two
  end
  
  it "has a link on the show page to edit a goal" do
    expect(page).to have_content 'Do tonights readings'
    expect(page).to have_link 'Edit Goal'
  end
  
  it "shows a form to edit the goal" do 
    click_link 'Edit Goal'
    expect(page).to have_field 'Name'
    expect(page).to have_content 'Private'
  end
  
  it "has all data pre-filled" do
    click_link 'Edit Goal'
    expect(find_field("Name").value).to have_content 'Do tonights readings'
    expect(page).to have_checked_field("true")
  end
  
  it "shows errors if editing fails" do
    click_link 'Edit Goal'
    fill_in 'Name', with: ''
    click_button 'Update Goal'
    expect(page).to have_content "Name can't be blank"
  end
  
  it "can't edit other users goals" do 
    batman_signup
    expect(page).not_to have_button 'Edit Goal'
  end
  
  it "redirects to goal show page on successful edit" do
    click_link 'Edit Goal'
    fill_in 'Name', with: 'Do practice assessment'
    click_button 'Update Goal'
    expect(page).to have_content 'Do practice assessment'
  end
  
  
end

feature "Delete Goals" do 
  before :each do
    make_two
  end
  
  it "has a delete button on show page" do 
    expect(page).to have_button 'Delete Goal'
  end
  
  it "removes goal on successful click" do
    click_button 'Delete Goal'
    expect(page).not_to have_content 'Do tonights readings'
    expect(page).to have_content 'Goals Index'
  end
  
  it "doesn't allow uses to delet other users goals" do 
    batman_signup
    click_link 'Do tonights readings'
    expect(page).not_to have_button 'Delete Goal'
  end

end



















