require "spec_helper"

feature "the signup process" do 
  before :each do
    visit 'users/new'
  end
  
  it "has a new user page" do
    expect(page).to have_content("Sign Up")
    expect(page).to have_field("Username")
    expect(page).to have_field("Password")
    expect(page).to have_button("Sign Up")
  end
  

  feature "signing up a user" do  
    it "shows username on the homepage after signup" do
      sign_up("helloworld", "password")
    end
  end

end

feature "logging in" do 
  before :each do
    visit 'users/new'
    sign_up("helloworld", "password")
    click_button "Sign Out"
    visit 'session/new'
  end

  it "shows username on the homepage after login" do
    expect(page).to have_content("Sign In")
    expect(page).to have_field("Username")
    expect(page).to have_field("Password")
    expect(page).to have_button("Sign In")
    sign_in("helloworld", "password")
  end
end

feature "logging out" do 
  before :each do
    visit 'users/new'
    sign_up("helloworld", "password")
    click_button "Sign Out"
  end

  it "begins with logged out state" do
    visit 'goals'
    expect(page).to have_button("Sign In")
  end

  it "doesn't show username on the homepage after logout" do
    visit 'session/new'
    sign_in("helloworld", "password")
    click_button "Sign Out"
    expect(page).to_not have_content("helloworld")
  end

end
