class FixSpellingInGoals < ActiveRecord::Migration
  def change
    remove_column :goals, :completition
    add_column :goals, :completion, :boolean
    change_column :goals, :completion, :boolean, null: false 
  end
end
