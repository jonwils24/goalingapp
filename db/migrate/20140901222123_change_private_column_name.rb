class ChangePrivateColumnName < ActiveRecord::Migration
  def change
    remove_column :goals, :private
    remove_column :goals, :completion
    add_column :goals, :privacy, :boolean
    add_column :goals, :completion, :boolean
    change_column :goals, :privacy, :boolean, null: false, default: false
    change_column :goals, :completion, :boolean, null: false, default: false
    
  end
end
